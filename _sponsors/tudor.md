---
name: Tudor
tier: gold
site_url: https://www.tudor.com
logo: tudor.svg
---
Tudor is an established macro and quantitative investment manager.  Python is central to our
business: we use Python to ingest billions of data points, run thousands of processes, and manage
millions of trades.  If you have an interest in financial markets, and know how to put Python and
other technology to work making sense of data, we want to talk to you.  Our teams are small and
agile, and make an outsized impact on our performance.
