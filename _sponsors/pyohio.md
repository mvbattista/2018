---
name: PyOhio
tier: community
site_url: https://www.pyohio.org/
logo: pyohio.png
---
The FREE Regional Python Conference for the Midwest and beyond! PyOhio is July 27-28, 2019 in
Columbus, OH. Get more info and sign up for email updates at <https://www.pyohio.org/>.
