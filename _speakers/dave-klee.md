---
name: Dave Klee
talks:
- "How to get started with Text Analysis in Python (and analyzing Donald Trump\u2019s
  tweets)."
---

Dave Klee is based in New York City and is excited to help others get started solving problems with Python. He has an MS in Information Management along with a BA in Broadcasting and a BS in Political Science.