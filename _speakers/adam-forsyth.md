---
name: Adam Forsyth
talks:
- 'Beating Mastermind: Winning Games, Translating Math to Code, and Learning from
  Donald Knuth'
---

I'm an Engineering and Community Lead @ Braintree. At work I do a variety of things from technical architecture and planning to facilitating conference and event sponsorships. Outside of work, I'm coding enthusiast and frequent traveler, splitting my time between the city and the Great North Woods.